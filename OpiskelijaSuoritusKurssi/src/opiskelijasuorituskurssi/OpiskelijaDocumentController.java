/**
 * @(#)OpiskelijaDocumentController.java
 * @author Hoang Tran
 * @Version 1.1 
 * Pvm: 29/03/2021
**/
package opiskelijasuorituskurssi;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *Opiskelija Controller class
 */
public class OpiskelijaDocumentController implements Initializable {

    @FXML
    private TextField txtOpiskelijaID;
    @FXML
    private TableView<Opiskelija> opiskelijaTable;
    @FXML
    private TableColumn<Opiskelija, Integer> OpiskelijaIDColumn; 
    
    @FXML
    private TableColumn<Opiskelija, String> SukunimiColumn;
    @FXML
    private TableColumn<Opiskelija, String> EtunimiColumn;
   
    @FXML
    private TableColumn<Opiskelija, String> EmailColumn;
    @FXML
    private TableColumn<Opiskelija, String> PuhelinColumn;
    @FXML
    private Button btLisä;
    @FXML
    private Button btMuokata;
    @FXML
    private Button btPoista;
    @FXML
    private Button btHaku;
    @FXML
    private TextField txtEtunimi;
    @FXML
    private TextField txtSukunimi;
    @FXML
    private TextField txtEmail;
    @FXML
    private TextField txtPuhelin;
    
    
    //Luoda connection, preparedStatemnet and ResultSet
    Connection con;
    PreparedStatement pst; 
    ResultSet rs;
    
    /**
     * Tietokanta yhteytää
     * tietokantanimi:OpiskelijaSuoristusKurssi
     * username:root
     * password:Talvi2020
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
     public void Connect() throws ClassNotFoundException, SQLException{
       try {Class.forName("org.mariadb.jdbc.Driver");
        con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/OpiskelijaSuoristusKurssi", "root", "Talvi2020");
        System.out.print("Database connected");
        } catch (SQLException e) {
            System.out.println("Database connect failed");
         }
    }
     
     
    /**
     * Luoda Kurssi lista 
     * @return opiskelijaList
     * @throws SQLException 
     * @throws java.lang.ClassNotFoundException 
     */
   
     public ObservableList<Opiskelija> getOpiskelijaList() throws SQLException, ClassNotFoundException{
        
         ObservableList<Opiskelija> opiskelijaList = FXCollections.observableArrayList();
         Connect();
         String sql = "SELECT * FROM opiskelija";
         Statement st;
         try { 
             st = con.createStatement();
             rs = st.executeQuery(sql);
            
            Opiskelija opis = null;
         
            while (rs.next()){
            opis.setOpiskelijaId(rs.getInt("opiskelijaId"));
            opis.setSukunimi(rs.getString("sukunimi"));
            opis.setEtunimi(rs.getString("etunimi"));
            opis.setEmail(rs.getString("email"));
            opis.setPuhelin(rs.getString("puhelin"));
            
            opiskelijaList.add(opis);
            }
            }catch(SQLException e){throw e;
            }
          return opiskelijaList;
    }    
    
     /**
     * Opiskelija talukko joka näytetään opiskelija lista
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    
    public void showOpiskelijaTable() throws ClassNotFoundException, SQLException{
        
        ObservableList<Opiskelija> list = getOpiskelijaList();

        OpiskelijaIDColumn.setCellValueFactory(new PropertyValueFactory<>("opiskelijaId"));
        SukunimiColumn.setCellValueFactory(new PropertyValueFactory<>("sukunimi"));
        EtunimiColumn.setCellValueFactory(new PropertyValueFactory<>("etunimi"));
        EmailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        PuhelinColumn.setCellValueFactory(new PropertyValueFactory<>("puhelin"));

        opiskelijaTable.setItems(list);
       
    }
     
   /**
     * Lisata tietokantaan opiskelijoita
     * @param event
     * @throws ClassNotFoundException
     * @throws SQLException 
     */

    @FXML
    private void Lisä(ActionEvent event) throws ClassNotFoundException, SQLException {
        Connect();
        String Etunimi = txtEtunimi.getText();
        String Sukunimi = txtSukunimi.getText();
        String Email = txtEmail.getText();  
        String Puhelin = txtPuhelin.getText(); 
         try {
            pst = con.prepareStatement("INSERT INTO Opiskelija (etunimi, sukunimi, email, puhelin) VALUES (?, ?, ?,?)");
            pst.setString(1, Etunimi);
            pst.setString(2, Sukunimi);
            pst.setString(3, Email);
            pst.setString(4, Puhelin);
              
            int status = pst.executeUpdate();
            
            if (status == 1) {
                Alert alert = new Alert (Alert.AlertType.INFORMATION);
                alert.setTitle ("Onnistu");
                alert.setHeaderText("Opiskelija");
                alert.setContentText("Lisä opiskelija onnistui. Tarkista lisää tietokannasta");
                alert.showAndWait();
                
               showOpiskelijaTable();
              
                txtEtunimi.setText("");
                txtSukunimi.setText("");
                txtEmail.setText("");
                txtPuhelin.setText("");
               
            
            }else {
                
                Alert alert = new Alert (Alert.AlertType.ERROR);
                alert.setTitle ("Ei onnistu");
                alert.setHeaderText("Opiskelija");
                alert.setContentText("Opiskelija lisä ei onnistu");
                alert.showAndWait();
             
            }
            
        }catch(SQLException e) {
                
        } 
        
    }
    
    /**
     * Muokata opiskelija tiedot ja päivitä tietokantaan
     * @param event
     * @throws SQLException
     * @throws ClassNotFoundException 
     */

    @FXML
    private void Muoka(ActionEvent event) throws ClassNotFoundException, SQLException {
        Connect();
       
        try {
        int opiskelijaId = Integer.parseInt(txtOpiskelijaID.getText());
        String sukunimi = txtSukunimi.getText();
        String etunimi = txtEtunimi.getText();
        String email = txtEmail.getText();  
        String puhelin = txtPuhelin.getText();

            pst = con.prepareStatement("UPDATE opiskelija SET sukunimi=?, etunimi=?, email=?  puhelin=? WHERE opiskelijaId=?");
            pst.setString(1, sukunimi);
            pst.setString(2, etunimi);
            pst.setString(3, email);
            pst.setString(4, puhelin);
            pst.setInt(5, opiskelijaId);
            

            int status = pst.executeUpdate();
            
            if (status == 1) {
                
                // Allert ikkuna 
                Alert alert = new Alert (Alert.AlertType.INFORMATION);
                alert.setTitle ("Onnistu");
                alert.setHeaderText("Opiskelija");
                alert.setContentText("Muoka opiskelija onnistui");
                alert.showAndWait();
                
                showOpiskelijaTable();
              
                txtEtunimi.setText("");
                txtSukunimi.setText("");
                txtEmail.setText("");
                txtPuhelin.setText("");
                
               
            
            }else {
                
                Alert alert = new Alert (Alert.AlertType.ERROR);
                alert.setTitle ("Ei onnistu");
                alert.setHeaderText("Opiskelija");
                alert.setContentText("Opiskelija muoka ei onnistu");
                alert.showAndWait();
             
            }   
        }catch (SQLException e) {throw e;}
    }
    
    /**
     * Poista opiskelija tiedot ja pävitä tietokantaan
     * @param event
     * @throws SQLException
     * @throws Exception 
     */
    @FXML
    private void Poista(ActionEvent event) throws ClassNotFoundException, SQLException, Exception {
        Connect();
        int opiskelijId = Integer.parseInt(txtOpiskelijaID.getText());
         
        try {
            pst = con.prepareStatement("DELETE FROM opiskelija WHERE opiskelijaId=?");
            
            pst.setInt(1, opiskelijId);
                          
            rs = pst.executeQuery();
            
             Alert alert = new Alert (Alert.AlertType.INFORMATION);
                alert.setTitle ("Onnistu");
                alert.setHeaderText("Opiskelija");
                alert.setContentText(" opiskelija poista onnistui");
                alert.showAndWait();  
            
            if (rs == null) { throw new Exception ("Opiskelija poistaminen ei onnistu");}
         } catch (SQLException e) {throw e;}
        
       showOpiskelijaTable();
    }
    
     @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            Connect();
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(OpiskelijaDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
       
}
