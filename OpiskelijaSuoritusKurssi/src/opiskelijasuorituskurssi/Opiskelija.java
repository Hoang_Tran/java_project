
package opiskelijasuorituskurssi;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author hoang
 */
public class Opiskelija {
    /**
     * Opiskelijan attribuuttit jotka vastaavat tietokantataulun sarakkeita
     */
    private final IntegerProperty opiskelijaId;
    private final StringProperty sukunimi;
    private final StringProperty etunimi; 
    private final StringProperty email;
    private final StringProperty puhelin;

    
    
    
    /**
     * Opiskelija constructori joka saa parametreina kaikki opiskelijan tiedot.
     */
    public Opiskelija(){
    opiskelijaId = new SimpleIntegerProperty(this, "opiskelijaId");
    etunimi = new SimpleStringProperty(this, "etunimi");
    sukunimi = new SimpleStringProperty(this, "sukunimi");
    email = new SimpleStringProperty(this, "email");  
    puhelin = new SimpleStringProperty(this, "puhelin"); 
    

}
    
    /**OpiskelijaId**/
    
    /**
     * Metodi palauta opiskelijaId
     * @return 
     */
    public IntegerProperty OpiskelijaIdProperty() {
        return opiskelijaId;
    }
    
    /**
     * Metodi palauta opiskelijaId
     * @return 
     */
    public int getOpiskelijaId(){return opiskelijaId.get();}
    
    /**
     * OpiskelijaId asetus
     * @param newOpiskelijaId 
     */
    
    public void setOpiskelijaId(int newOpiskelijaId){
        opiskelijaId.set(newOpiskelijaId);
    }
    
    /**Etunimi**/
    
    /**
     * Metodi palauta opiskelija etunimi
     * @return etunimi
     */
    public StringProperty EtunimiProperty() {
        return etunimi;
    }
    
   /**
    * Metodi palauta opiskelija etunimi
    * @return 
    */
    public String getEtunimi(){return etunimi.get();}
    
    /**
     * Opiskelijan etunimi asetus
     * @param newEtunimi 
     */
    
    public void setEtunimi(String newEtunimi){
      etunimi.set(newEtunimi);
    }
    
    
    /**Opiskelijan sukunimi **/
    
    /**
     * Metodi palauta opiskelija sukunimi
     * @return 
     */

    public StringProperty SukunimiProperty() {
        return sukunimi;
    }
    
   /**
    * Metodi palauta opiskelija sukunimi
    * @return sukunimi
    */
    
    public String getSukunimi(){return sukunimi.get();}
    
    /**
     * Opiskelija sukunimi asetus
     * @param newSukunimi 
     */
    
    public void setSukunimi(String newSukunimi){
       sukunimi.set(newSukunimi);
    }
    
    /** Opiskleija email**/
    
    /**
     * Metodi palauta opiskelija email
     * @return email
     */
    public StringProperty EmailProperty() {
        return email;
    }
    
    /**
     * Metodi palauta opiskelija email
     * @return email
     */
    
    public String getEmail(){return email.get();}
    
    /**
     * Opiskelija Emai asetus
     * @param newEmail 
     */
    
    public void setEmail(String newEmail){
        email.set(newEmail);
    }
    
    
    /** Opiskelijan puhelin numero**/
    
    /**
     * Meotodi palauta opiskelijan puhelin numero
     * @return 
     */
    public StringProperty PuhelinProperty() {
        return puhelin;
    }
    
    /**
     * Metodi palauta opiskelijan puhelin numero
     * @return 
     */
    public String getPuhelin(){return puhelin.get();}
    
    /**
     * Opiskelijan puhelin numero asetus
     * @param newPuhelin 
     */
    public void setPuhelin(String newPuhelin){
        puhelin.set(newPuhelin);
    }
 
    /**
     * Metodi palauta opiskelija kaikki tiedot: opsikelijaid, etunimi, sukunimi, email, puhelin 
     * @return 
     */

    @Override
    public String toString() {
		return (opiskelijaId + "  " + etunimi  + "  " +  sukunimi + "  " + email + " " + puhelin + "");
		} 

}